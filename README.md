Your data directory needs to have the appropriate permissions based on the UID of the container

graylog:
    graylog: uid=1100 gid=1100

mongo:
    mongodb: uid=999 gid=999

elasticsearch:
   elasticsearch: uid=105 gid=108
 
