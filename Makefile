COMPOSEBIN := $(shell which docker-compose)
DOCKERBIN := $(shell which docker)

all: check build

check:
ifndef COMPOSEBIN
	$(error docker-compose binary not found in PATH)
endif
ifndef DOCKERBIN
	$(error docker binary not found in PATH)
endif

help:
	$(info Targets:)
	$(info     build: docker-compose up and refresh ca-certificates)
	$(info     clean: kill and remove containers/volumes)

build:
	$(COMPOSEBIN) create
	$(COMPOSEBIN) ps -a

clean: check
	$(COMPOSEBIN) kill   # Stop running container
	$(COMPOSEBIN) rm -fv # Remove containers and volumes
	$(DOCKERBIN) rmi $(shell $(DOCKERBIN) images -aqf 'dangling=true') # Remove dangling/unused images

